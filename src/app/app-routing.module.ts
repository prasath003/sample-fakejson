import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './home/home.component';
import {EndComponent} from './end/end.component';
import {NavbarComponent} from './navbar/navbar.component';
import {AuthorizeGuard} from './guard/authorize.guard';
import {WebsiteComponent} from './dialog/website.component';
import {WebsiteregisterComponent} from './websiteregister/websiteregister.component';

const routes: Routes = [{
  path: '',
  component: HomeComponent
}, { path: 'end',
  canActivate: [AuthorizeGuard],
  component: EndComponent,
  data : {admin: 'admin'}
}, {
  path: 'nav',
  canActivate: [AuthorizeGuard],
  component: NavbarComponent,
  data: { admin: 'admin' }
},{
  path: 'dialog',
  canActivate: [AuthorizeGuard],
  component: WebsiteComponent,
  data: { admin: 'admin' }
},{
  path: 'web',
  canActivate: [AuthorizeGuard],
  component: WebsiteregisterComponent,
  data: { admin: 'admin' }
}];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
