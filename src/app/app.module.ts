import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import {HttpClientModule} from '@angular/common/http';
import { EndComponent } from './end/end.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatToolbarModule,
  MatButtonModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatGridListModule,
  MatCardModule,
  MatMenuModule,
  MatProgressBarModule,
  MatInputModule,
  MatTableModule, MatPaginatorModule, MatSortModule, MatFormFieldModule, MatDatepickerModule, MatNativeDateModule, MatDialogModule
} from '@angular/material';
import { WebsiteComponent } from './dialog/website.component';
import { NavbarComponent } from './navbar/navbar.component';
import {AuthorizeGuard} from './guard/authorize.guard';
import { WebsiteregisterComponent } from './websiteregister/websiteregister.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    EndComponent,
    WebsiteComponent,
    NavbarComponent,
    WebsiteregisterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatProgressBarModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatDialogModule
  ],
  entryComponents: [
    WebsiteComponent
  ],
  providers: [AuthorizeGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
