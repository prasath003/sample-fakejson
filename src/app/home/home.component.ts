import { Component, OnInit } from '@angular/core';
import {DataService} from '../services/data.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  name: string;
  address: string;
  saveResult: any;
  getResult: any;
  constructor(private dataService: DataService, private router: Router) { }

  ngOnInit() {
    this.getData();
  }

  async saveData(name, address) {
    this.saveResult = await this.dataService.saveUserData(name, address);
    console.log(this.saveResult);
    this.getData();
  }
  async getData() {
    this.getResult = await this.dataService.getUserData();
    console.log(this.getResult);
  }
  redirect() {
    this.router.navigateByUrl('/end');
  }

}
