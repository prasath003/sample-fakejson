import { Injectable } from '@angular/core';
import {HttpHeaders, HttpClient, HttpResponse} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }
  saveUserData(name, address) {
    interface UserResponse {
      success: boolean;
      dataset: any;
    }
    const headerKey = {'Content-Type': 'application/json', 'Accept': '', 'Authkey': ''};
    const headers = new HttpHeaders(headerKey);

    const body = JSON.stringify({
      'name': name,
      'address': address,
      'author': 'fromapp'
    });

    return new Promise(resolve => {
      this.http.post('http://localhost:3000/userdata', body, {headers: headers, observe: 'response'})
        .subscribe((response: HttpResponse<UserResponse>) => {
            if (response.status === 200) {
                resolve();
              } else {
                resolve(false);
              }
          }, error => {
            resolve(false);
          }
        );
    });
  }
  getUserData() {
    interface UserResponse {
      success: boolean;
      dataset: any;
    }
    const headerKey = {'Content-Type': 'application/json', 'Accept': '', 'Authkey': ''};
    const headers = new HttpHeaders(headerKey);

    /*const body = JSON.stringify({
      'name': name,
      'address': address,
      'author': 'fromapp'
    });*/

    return new Promise(resolve => {
      this.http.get('http://localhost:3000/userdata', {headers: headers, observe: 'response'})
        .subscribe((response: HttpResponse<UserResponse>) => {
            if (response.status === 200) {
                resolve(response.body);
              } else {
                resolve(false);
              }
          }, error => {
            resolve(false);
          }
        );
    });
  }
}
